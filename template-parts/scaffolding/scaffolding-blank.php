<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Hashim
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'john-hashim' ); ?></h2>
</section>
