<?php
/**
 * The template used for displaying Buttons in the scaffolding library.
 *
 * @package Hashim
 */

?>

<section class="section-scaffolding">

	<h2 class="scaffolding-heading"><?php esc_html_e( 'Buttons', 'john-hashim' ); ?></h2>
	<?php
		// Button.
		john_hashim_display_scaffolding_section( array(
			'title'       => 'Button',
			'description' => 'Display a button.',
			'usage'       => '<button class="button" href="#">Click Me</button>',
			'output'      => '<button class="button">Click Me</button>',
		) );
	?>
</section>
