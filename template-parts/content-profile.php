<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hashim
 */

?>
<div class="container">
	<div class="profile-header">
		<div class="info">
			<h1>Amli</h1>
			<p>Learn &amp; Learn<br><br><span class="place">Indonesia</span></p>
		</div>
	</div>	
	<div class="details">
		<h2>About</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nec sodales ipsum. Donec sit amet mi ac risus commodo egestas.</p>
		<div class="social-media">
			<div class="col_1"></div>
			<div class="col_2"></div>
			<div class="col_3"></div>
		</div>
	</div>
</div>

<!--This Is Not Part Of Profile Card #2-->
<a class="me" href="https://codepen.io/uzcho_/pens/popular/?grid_type=list" target="blank_">
	<span>My Pens List</span>
</a>
